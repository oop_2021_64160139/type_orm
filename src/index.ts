import { AppDataSource } from "./data-source"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const UserRepository = AppDataSource.getRepository(User);
    const user = new User()
    user.Login = "User1"
    user.name = "User1"
    user.Password = "Pass@123"
    await UserRepository.save(user)
    console.log("Saved a new user with id: " + user.id)

    console.log("Loading users from the database...")
    const users = await UserRepository.find()
    console.log("Loaded users: ", users)


}).catch(error => console.log(error))
